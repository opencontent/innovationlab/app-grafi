import { Component, ElementRef, NgZone, OnDestroy, OnInit, ViewChild } from '@angular/core';
import {
  icon,
  LatLngExpression,
  Map,
  Marker,
  tileLayer,
  marker,
  PopupOptions,
  DomEvent,
  DomUtil,
  Polyline
} from 'leaflet';
import { Apollo } from 'apollo-angular';
import { DomSanitizer } from '@angular/platform-browser';
import { LoadingController, ModalController, ToastController } from '@ionic/angular';
import { ZoneService } from '../../services/zone.service';
import { Geolocation, Position } from '@capacitor/geolocation';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ElementService } from '../../services/element.service';
import { StorageService } from '../../services/storage.service';
import { Router } from '@angular/router';
import * as L from 'leaflet';
import 'leaflet.markercluster';
import { Control } from 'leaflet';

@Component({
  selector: 'app-eco-points-details',
  templateUrl: './elements-map.page.html',
  styleUrls: ['./elements-map.page.scss'],
})

export class ElementsMapPage implements OnInit, OnDestroy {
  error: any;
  label = 'Seleziona elemento sulla mappa';
  position: Position = null;
  currentPos: GeolocationPosition;
  waitingForCurrentPosition = false;
  @ViewChild('map', { read: ElementRef, static: true }) mapElement: ElementRef;
  @ViewChild('one', { static: false }) d1: ElementRef;
  municipalityData: any;
  private content: any;
  private geocode: LatLngExpression = [0, 0];
  private marker: Marker;
  private mapRef: Map;
  private unsubscribe$ = new Subject<void>();
  private positionMarkersArray: any[] = [];
  private token: string;
  private sizePoles: any;
  private polyline: Polyline<any>;

  constructor(
    private apollo: Apollo,
    public sanitizer: DomSanitizer,
    public modalController: ModalController,
    public ngZone: NgZone,
    private zoneService: ZoneService,
    private polesService: ElementService,
    public loadingController: LoadingController,
    public toastController: ToastController,
    private storage: StorageService,
    private router: Router
  ) {}

  ngOnInit() {
    this.storage.getToken().then((res) => {
      if (res) {
        this.token = JSON.parse(res).token;
      } else {
        this.token = null;
      }
    });

    this.storage.getMunicipality().then((res) => {
      this.municipalityData = JSON.parse(res) || null;

      this.polesService
        .getElement(this.municipalityData.comune)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(
          (result) => {
            this.content = result;
            this.sizePoles = result.length
            if (this.mapRef !== undefined) { this.mapRef.remove(); }
            if(result.length > 0){
              this.initMap(this.sizePoles);
            }else{
              this.error = 'Nessun elemento trovato per questo comune';
            }

          },
          (error) => {
            this.error = 'Nessun elemento trovato per questo comune';
          }
        );
    });
  }

  ionViewWillEnter() {
    this.storage.getToken().then((res) => {
      if (res) {
        this.token = JSON.parse(res).token;
      } else {
        this.token = null;
      }
    });

    this.storage.getMunicipality().then((res) => {
      this.municipalityData = JSON.parse(res) || null;
      this.polesService
        .getElement(this.municipalityData.comune)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(
          (result) => {
            this.content = result;
            this.sizePoles = result.length
            if (this.mapRef !== undefined) { this.mapRef.remove(); }
            if(result.length > 0){
              this.initMap(this.sizePoles);
            }else{
              this.error = 'Nessun elemento trovato per questo comune';
            }
          },
          (error) => {
            this.error = 'Nessun elemento trovato per questo comune';
          }
        );
    });
  }

  initMap(sizePoles: any) {
    this.mapRef= new Map(this.mapElement.nativeElement, { zoomControl: false, tap: false  }).setView(this.geocode, 10);
    tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
    }).addTo(this.mapRef);

    L.control.zoom({ position: 'topright' }).addTo(this.mapRef);

    if (this.content && this.content.length > 0) {

      this.content.forEach((el) => {
        let latlngs: LatLngExpression[] = el.LineString
        this.positionMarkersArray.push(latlngs);
        const loc = el.the_geom;
        // create popup contents
        let customPopup = `
<div><h4>Descrizione</h4><div><p>
<strong>Comune</strong>: ${el.comune}<br>
<strong>Nome</strong>: ${el.nome}<br>
<strong>Cod</strong>: ${el.codvia}<br>
<strong>Tipologia</strong>: ${el.tipologia}<br>
</p>
</div></div>`;

        if (this.token) {
          const ctaButton =
              '<ion-button color="primary" class="d-block" id="' +
              loc.toString() +
              '">\n' +
              '  <ion-label>\n' +
              '    Invia segnalzione\n' +
              '  </ion-label>\n' +
              '</ion-button>';
          customPopup += ctaButton;
        }

        // specify popup options
        const customOptions: PopupOptions = {
          maxWidth: 500,
          maxHeight: 400,
          className: 'layer-popup',
        };

        // zoom the map to the polyline
        this.polyline = L.polyline(this.positionMarkersArray, {color: 'red'})
            .togglePopup()
            .bindPopup(customPopup, customOptions)
            .on('click', (event) => {
              const positionStringify = loc.toString();
              const data = this.searchDataMarker(el.the_geom) || null;
              const buttonSubmit = DomUtil.get(positionStringify);
              if (buttonSubmit) {
                L.DomEvent.addListener(buttonSubmit, 'click', (ee) => {
                  DomEvent.stopPropagation(ee);
                  this.router.navigate(['new-post-elements'], {state: {dataDetails: data, address: null}});
                });
              }
            })

      })

      this.polyline.addTo(this.mapRef);
      this.mapRef.fitBounds(this.polyline.getBounds());
    }

/*    const legend = new Control({position: 'topleft'});

    legend.onAdd = (map) =>  {
      const div = L.DomUtil.create('div', 'info legend');
      div.innerHTML += `<b>Totale segnaletiche: ${sizePoles || null}</b>`;
      return div;
    };

    legend.addTo(this.mapRef);*/

  }

  searchDataMarker(searchData: string) {
    return this.content.filter((el) => el.the_geom.toString() === searchData);
  }

  centerLeafletMapOnMarker(map, marker) {
    const latLngs = [marker.getLatLng()];
    const markerBounds = L.latLngBounds(latLngs);
    map.fitBounds(markerBounds);
  }

  updateLabelMarkerStatic(position: any) {
    this.zoneService.addressLookup([position.lat, position.lng]).subscribe((res) => {
      const data = res.replace(/cb\(/g, '');
      const jsonData = JSON.parse(data.replace(/\)/g, ''));
      this.label = jsonData.display_name;
    });
  }

  updateLabelMarker(position: any) {
    this.zoneService.addressLookup([position.lat, position.lng]).subscribe((res) => {
      const data = res.replace(/cb\(/g, '');
      const jsonData = JSON.parse(data.replace(/\)/g, ''));
      this.label = jsonData.display_name;
      this.geocode = [position.lat, position.lng];
      this.marker.setPopupContent(`<b>${jsonData.display_name}</b>`).openPopup();
    });
  }

  async requestPermissions() {
    const permResult = await Geolocation.requestPermissions();
    console.log('Perm request result: ', permResult);
  }


  async presentToastWithOptions() {
    const toast = await this.toastController.create({
      header: 'Geocalizazzione non abilitata',
      message: '',
      position: 'top',
      buttons: [
        {
          text: 'Chiudi',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          },
        },
      ],
    });
    await toast.present();

    const { role } = await toast.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }

  ngOnDestroy(): void {
    // destroy instance map
    setTimeout(_=>{
      this.mapRef.off();
      this.mapRef.remove();
    }, 200)

    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
